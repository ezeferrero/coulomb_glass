*CUDA Coulomb Glass*  

coulomb_glass.cu is a CUDA implementation of a 2D Coulomb Glass model.  
The program simulates the Kinetic Monte Carlo dynamics of a gas of electrons hopping in a 2D square lattice with randomly distributed (quenched) energy traps.  
This program implements the "Parallel Rejection" method described in our paper.  
Started on: Jan, 2013 by ezeferrero  
Copyright (C) 2014 Ezequiel E. Ferrero, Alejandro B. Kolton and Matteo Palassini.  

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

This code was originally implemented for: "Parallel kinetic Monte Carlo simulation of Coulomb glasses",  
E.E. Ferrero, A.B. Kolton and M. Palassini  
http://arxiv.org/abs/1407.5026  
AIP Conference Proceedings XX, XX (2014)  
Proceedings of the TIDS15 Conference, September 2013.  
**Please cite when appropriate**.  